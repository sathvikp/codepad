Social Coding Made Simple.

http://notepyd.appspot.com/

LICENCE: http://creativecommons.org/licenses/by-nc-sa/3.0/

Got something to say? Speak out at https://groups.google.com/forum/#!forum/codepad or open an issue here on GitHub



---------------------------------------
Like our [work](http://doersguild.com)? [Get in touch!](mailto:mail@doersguild.com)
